import { NgModule } from '@angular/core';
import { LayoutModule } from '@angular/cdk/layout';
import { ReactiveFormsModule } from '@angular/forms';
import {MaterialModule} from './material-module';
import {ConfigService} from './service/config.service';
import { WebsocketService } from './service/websocket.service';
import { Ps4GamepadService } from './service/ps4-gamepad.service'
import { PwmService} from './service/pwm.service';
import { FormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  exports: [
    MaterialModule,
    LayoutModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
      ConfigService,
      WebsocketService,
      PwmService,
      Ps4GamepadService
  ],

})

export class SharedModule {

}
