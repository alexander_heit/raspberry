export interface PwmCalibration{
    minIn:number;
    maxIn:number;
    minOut:number;
    maxOut:number;
}

export class PwmClass {

    constructor(
        public valueIn:number = 0,
        public minIn: number = 0,
        public maxIn: number = 100,
        public minOut: number = 0.01,
        public maxOut: number = 0.99) {
    }
    
    public get valueOut(){
        const value = ( (this.valueIn - this.minIn) * (this.maxOut - this.minOut) / (this.maxIn - this.minIn) + this.minOut);
        return value;
    }

    public static map(valueIn:number, calibration:PwmCalibration){
        return ( 
            (valueIn - calibration.minIn) * (calibration.maxOut - calibration.minOut) / 
            (calibration.maxIn - calibration.minIn) + calibration.minOut);
    }
}
