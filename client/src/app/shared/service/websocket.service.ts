import { Injectable } from '@angular/core';
import { Subject, Observable, Observer } from 'rxjs';
import { ConfigService } from './config.service';
import { map, retry } from 'rxjs/operators';


@Injectable()
export class WebsocketService {
    constructor(private configService: ConfigService) { }

    private subject: Subject<any>;
    private ws;
    public connect(endpoint:string): Subject<any> {
        if (this.subject) {
            this.close();
        }
        this.subject = this.create(this.configService.webSocketUrl + '/' + endpoint);
        return this.subject;
    }

    public close(){
        if (this.subject) {
            this.subject.unsubscribe();
            this.subject.complete();
        }

        if(this.ws){
            this.ws.close();
        }        
    }

    public getWebsocket(){
        return this.ws;
    }
    
    private create(url): Subject<any> {

        this.ws = new WebSocket(url);

        const observable = Observable.create((obs: Observer<any>) => {
            this.ws.onmessage = obs.next.bind(obs);
            this.ws.onerror = obs.error.bind(obs);
            this.ws.onclose = obs.complete.bind(obs);
            return this.ws.close.bind(this.ws);
        });

        const observer = {
            next: (data) => {
                if (this.ws.readyState === WebSocket.OPEN) {
                    this.ws.send(data);
                }
            }
        };

        return Subject.create(observer, observable);
    }
}
