import { Injectable, OnDestroy } from '@angular/core';
import { Observable, interval} from 'rxjs';
import { map } from 'rxjs/operators';
import {Ps4GamepadDto} from '../../../../../shared-class/ps4-gamepad.dto';

@Injectable()
export class Ps4GamepadService{

    constructor(){

    }

    get(timeInterval:number = 60){
        return interval(timeInterval).pipe(map(() => {
            return new Ps4GamepadDto(navigator.getGamepads()[0]);
        }));
    }    

}
