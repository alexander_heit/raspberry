import { Injectable } from '@angular/core';

export interface Navigation {
    title: string;
    url: string;
}

@Injectable()
export class ConfigService {

    public readonly title: string = 'Raspberry Fun';
    public readonly webSocketUrl = 'ws://raspberrypi:3000';
    public readonly navigation: Array<Navigation> = [
        {title: 'Websocket Hello World', 'url': '/websocket-hello'},
        {title: 'Gpio test', 'url': '/gpio-test'},
        {title: 'PWM Kalibrierung', 'url': '/pwm-calibration'},
        {title: 'Ps4 Gamepad', 'url': 'ps4-gamepad-test'},
        {title: 'Webcam', 'url': '/webcam'},
        {title: 'WLAN Car', 'url': '/wlan-car'},
    ];

    constructor() {

    }

}
