import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SharedModule} from './shared/shared.module';
import { WebsocketHelloComponent } from './routing/websocket-hello/websocket-hello.component';
import { WlanCarComponent } from './routing/wlan-car/wlan-car.component';
import { WelcomeComponent } from './routing/welcome/welcome.component';
import { PwmCalibrationComponent } from './routing/pwm-calibration/pwm-calibration.component';
import { Ps4ControllerCalibrationComponent } from './routing/ps4-controller-calibration/ps4-controller-calibration.component';
import { WebcamComponent } from './routing/webcam/webcam.component';
import { GpioTestComponent } from './gpio-test/gpio-test.component';

@NgModule({
  declarations: [
    AppComponent,
    WebsocketHelloComponent,
    WlanCarComponent,
    WelcomeComponent,
    PwmCalibrationComponent,
    Ps4ControllerCalibrationComponent,
    WebcamComponent,
    GpioTestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

}
