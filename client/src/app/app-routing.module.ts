import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WebsocketHelloComponent } from './routing/websocket-hello/websocket-hello.component';
import { WlanCarComponent } from './routing/wlan-car/wlan-car.component';
import { WelcomeComponent } from './routing/welcome/welcome.component';
import { PwmCalibrationComponent } from './routing/pwm-calibration/pwm-calibration.component';
import { Ps4ControllerCalibrationComponent } from './routing/ps4-controller-calibration/ps4-controller-calibration.component';
import { WebcamComponent } from './routing/webcam/webcam.component';
import { GpioTestComponent } from './gpio-test/gpio-test.component';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/gpio-test',
  },
  {
    path: 'websocket-hello',
    component: WebsocketHelloComponent
  },
  {
    path: 'welcome',
    component: WelcomeComponent
  },
  {
    path: 'webcam',
    component: WebcamComponent
  },
  {
    path: 'pwm-calibration',
    component: PwmCalibrationComponent
  },
  {
    path: 'ps4-gamepad-test',
    component: Ps4ControllerCalibrationComponent
  },
  {
    path: 'gpio-test',
    component: GpioTestComponent
  },
  {
    path: 'wlan-car',
    component: WlanCarComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
