import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpioTestComponent } from './gpio-test.component';

describe('GpioTestComponent', () => {
  let component: GpioTestComponent;
  let fixture: ComponentFixture<GpioTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpioTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpioTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
