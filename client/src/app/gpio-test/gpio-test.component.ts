import { Component, OnInit } from '@angular/core';

export interface GpioPin{
  physical:number;
  bcm:number;
  selected: boolean;
}

@Component({
  selector: 'app-gpio-test',
  templateUrl: './gpio-test.component.html',
  styleUrls: ['./gpio-test.component.scss']
})
export class GpioTestComponent implements OnInit {


  public gpio:GpioPin[] = [
    {physical: 28, bcm: 1, selected: false},
    {physical: 3, bcm: 2, selected: false},
    {physical: 5, bcm: 3, selected: false},
    {physical: 7, bcm: 4, selected: false},
    {physical: 29, bcm: 5, selected: false},
    {physical: 31, bcm: 6, selected: false},
    {physical: 26, bcm: 7, selected: false},
    {physical: 24, bcm: 8, selected: false},
    {physical: 21, bcm: 9, selected: false},
    {physical: 19, bcm: 10, selected: false},
    {physical: 23, bcm: 11, selected: false},
    {physical: 32, bcm: 12, selected: false},
    {physical: 33, bcm: 13, selected: false},
    {physical: 8, bcm: 14, selected: false},
    {physical: 10, bcm: 15, selected: false},
    {physical: 36, bcm: 16, selected: false},
    {physical: 11, bcm: 17, selected: false},
    {physical: 12, bcm: 18, selected: false},
    {physical: 35, bcm: 19, selected: false},
    {physical: 38, bcm: 20, selected: false},
    {physical: 40, bcm: 21, selected: false},
    {physical: 15, bcm: 22, selected: false},
    {physical: 16, bcm: 23, selected: false},
    {physical: 18, bcm: 24, selected: false},
    {physical: 22, bcm: 25, selected: false},
    {physical: 37, bcm: 26, selected: false},
  ];

  constructor() { 

  }

  ngOnInit() {
  
  }

}
