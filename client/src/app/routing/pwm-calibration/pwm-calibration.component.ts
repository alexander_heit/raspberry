import { Component, OnInit } from '@angular/core';
import { WebsocketService } from '../../shared/service/websocket.service';
import { PwmClass } from '../../shared/class/pwm.class';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-pwm-calibration',
  templateUrl: './pwm-calibration.component.html',
  styleUrls: ['./pwm-calibration.component.scss']
})
export class PwmCalibrationComponent implements OnInit {

  public pwm:PwmClass;
  public step:Number = 5;
  private websocket:Subject<MessageEvent>;

  constructor(websocketService:WebsocketService) { 
    this.websocket = websocketService.connect('pwm-calibration');
    this.pwm = new PwmClass();
    this.pwm.valueIn = 10;
  } 

  ngOnInit() {
  }

  change(event){
    this.pwm.valueIn = event.value;
    this.websocket.next(<any>this.pwm.valueOut);
  }

}
