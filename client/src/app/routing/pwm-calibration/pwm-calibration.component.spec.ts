import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PwmCalibrationComponent } from './pwm-calibration.component';

describe('PwmCalibrationComponent', () => {
  let component: PwmCalibrationComponent;
  let fixture: ComponentFixture<PwmCalibrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PwmCalibrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PwmCalibrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
