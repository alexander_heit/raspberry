declare var WSAvcPlayer: any;

import { Component, OnDestroy, AfterViewInit } from '@angular/core';
import { WebsocketService } from 'src/app/shared/service/websocket.service';
import { WebcamConfigDto, WebcamConfigInterface } from '../../../../../shared-class/webcam.dto';

export enum WebcamStateEnum {
  ready = 'ready',
  error = 'error'
}

export interface WebcamStateInterface {
  state: WebcamStateEnum;
  param?: any;
}

@Component({
  selector: 'app-webcam',
  templateUrl: './webcam.component.html',
  styleUrls: ['./webcam.component.scss']
})
export class WebcamComponent implements OnDestroy, AfterViewInit {

  private webSocketSubject;
  public commandReady: Boolean = false;
  public streamReady: Boolean = false;
  public webcamConfig: WebcamConfigInterface = new WebcamConfigDto();
  public wsavc = null;

  /*
  exposure?:ExposureEnum; // : Set exposure mode (see Notes)
awb?:AwbEnum; // : Set AWB mode (see Notes)
imxfx?:ImxfxEnum; // : Set image effect (see Notes)
metering?:MeteringEnum; // : Set metering mode (see Notes)
*/
  public exposureSelect = [
    { value: 'auto', viewValue: 'auto' },
    { value: 'night', viewValue: 'night' },
    { value: 'nightpreview', viewValue: 'nightpreview' },
    { value: 'backlight', viewValue: 'backlight' },
    { value: 'spotlight', viewValue: 'spotlight' },
    { value: 'sports', viewValue: 'sports' },
    { value: 'snow', viewValue: 'snow' },
    { value: 'beach', viewValue: 'beach' },
    { value: 'verylong', viewValue: 'verylong' },
    { value: 'fixedfps', viewValue: 'fixedfps' },
    { value: 'antishake', viewValue: 'antishake' },
    { value: 'fireworks', viewValue: 'fireworks' }
  ];

  public awbSelect = [
    { value: 'off', viewValue: 'off' },
    { value: 'auto', viewValue: 'auto' },
    { value: 'sun', viewValue: 'sun' },
    { value: 'cloud', viewValue: 'cloud' },
    { value: 'shade', viewValue: 'shade' },
    { value: 'tungsten', viewValue: 'tungsten' },
    { value: 'fluorescent', viewValue: 'fluorescent' },
    { value: 'incandescent', viewValue: 'incadescent' },
    { value: 'flash', viewValue: 'flash' },
    { value: 'horizon', viewValue: 'horizon' }
  ];

  public imfxSelect = [
    { value: 'none', viewValue: 'none' },
    { value: 'negative', viewValue: 'negative' },
    { value: 'solarise', viewValue: 'solarise' },
    { value: 'sketch', viewValue: 'sketch' },
    { value: 'denoise', viewValue: 'denoise' },
    { value: 'emboss', viewValue: 'emboss' },
    { value: 'oilpaint', viewValue: 'oilpaint' },
    { value: 'hatch', viewValue: 'hatch' },
    { value: 'gpen', viewValue: 'gpen' },
    { value: 'pastel', viewValue: 'pastel' },
    { value: 'watercolour', viewValue: 'watercolour' },
    { value: 'film', viewValue: 'film' },
    { value: 'blur', viewValue: 'blur' },
    { value: 'saturation', viewValue: 'saturation' },
    { value: 'colourswap', viewValue: 'colourswap' },
    { value: 'washedout', viewValue: 'washedout' },
    { value: 'posterise', viewValue: 'posterise' },
    { value: 'colourpoint', viewValue: 'colourpoint' },
    { value: 'colourbalance', viewValue: 'colurbalance' },
    { value: 'cartoon', viewValue: 'cartoon' }
  ];

  public meteringSelect = [
    { value: 'average', viewValue: 'average' },
    { value: 'spot', viewValue: 'spot' },
    { value: 'backlit', viewValue: 'backlit' },
    { value: 'matrix', viewValue: 'matrix' }
  ];

  constructor(private webSocketService: WebsocketService) {
    this.connect();
  }

  connect() {
    this.webSocketSubject = this.webSocketService.connect('webcam/command');
    this.webSocketSubject.subscribe(message => {
      console.debug('message', message);
      const response: WebcamStateInterface = JSON.parse(message.data);
      console.debug('response', response);
      if (response.state === WebcamStateEnum.ready) {
        this.streamReady = true;
        console.log('DOM fully loaded and parsed');
        const canvas = document.getElementById('videostream');
        console.debug('canvas', canvas);
        if(this.wsavc){
          this.wsavc.disconnect();
        }
        this.wsavc = new WSAvcPlayer(canvas, 'webgl');
        this.wsavc.connect('ws://raspberrypi:3000/webcam/stream');
      }
      else {
        this.streamReady = false;
      }
    },
      (err) => {
        this.commandReady = false;
      },
      () => {
        this.commandReady = false;
      });
  }

  close() {
    this.streamReady = false;
    this.commandReady = false;
    this.wsavc.disconnect();
    this.webSocketService.close();
  }

  ngOnDestroy() {
    this.webSocketService.close();
  }

  send() {
    this.webSocketSubject.next(JSON.stringify(this.webcamConfig));
  }

  ngAfterViewInit() {
    /*
    document.addEventListener('DOMContentLoaded', function(event) {
      console.log('DOM fully loaded and parsed');
      const canvas = document.getElementById('videostream');
      console.debug('canvas', canvas);
      const wsavc = new WSAvcPlayer(canvas, 'webgl');
      wsavc.connect('ws://raspberrypi:3000/webcam/stream');
  });*/
  }

}
