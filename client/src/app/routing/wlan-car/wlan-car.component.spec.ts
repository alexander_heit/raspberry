import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WlanCarComponent } from './wlan-car.component';

describe('WlanCarComponent', () => {
  let component: WlanCarComponent;
  let fixture: ComponentFixture<WlanCarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WlanCarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WlanCarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
