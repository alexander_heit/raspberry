import { Component, OnDestroy } from '@angular/core';
import { Ps4GamepadService } from '../../shared/service/ps4-gamepad.service';
import { Subscription } from 'rxjs';
import { Ps4GamepadDto } from '../../../../../shared-class/ps4-gamepad.dto';
import { WebsocketService } from '../../shared/service/websocket.service';

import { webSocket } from 'rxjs/webSocket';

import { TankControlDto, MotorDirectionEnum, MotorWheelInterface } from '../../../../../shared-class/tankcontrol.dto';

@Component({
  selector: 'app-wlan-car',
  templateUrl: './wlan-car.component.html',
  styleUrls: ['./wlan-car.component.scss']
})
export class WlanCarComponent implements OnDestroy {

  public hit:any;
  public tankControl: TankControlDto = new TankControlDto();
  private ps4GamepadSubscription: Subscription;


  constructor(private ps4GamepadService: Ps4GamepadService, websocketService: WebsocketService) {

    const command$ = webSocket('ws://raspberrypi:3000/wlan-car/command');
    command$.subscribe(
      (msg) => {
        if (msg !== 'ready') {
          return;
        }
        this.tankControl.gun.y = 0.1;
        this.tankControl.gun.x = 0.1;

        this.ps4GamepadSubscription = this.ps4GamepadService.get(120).subscribe((gamepad: Ps4GamepadDto) => {
          this.tankControl.motor.left = this.convertMotorSpeed(gamepad.axe.left.y);
          this.tankControl.motor.right = this.convertMotorSpeed(gamepad.axe.right.y);

          this.tankControl.gun.x = this.convertGunYPosition({
            currentPosition: this.tankControl.gun.x,
            defaultPosition: 0.1,
            up: gamepad.button.right,
            down: gamepad.button.left,
            min: 0.05,
            max: 0.19,
            steps: 0.001,
            default: gamepad.button.triangle
          });

          this.tankControl.gun.y = this.convertGunXPosition({
            currentPosition: this.tankControl.gun.y,
            defaultPosition: 0.1,
            left: gamepad.trigger.l2,
            right: gamepad.trigger.r2,
            min: 0.05,
            max: 0.185,
            factor: 0.005,
            default: gamepad.button.circle
          });

          this.tankControl.gun.fire = gamepad.button.x;
          command$.next(this.tankControl);

        });
      },
      (err) => console.debug('error', err));

    const hit$ = webSocket('ws://raspberrypi:3000/wlan-car/hit');
    hit$.subscribe(
      (message) => {
        this.hit = message;
      },
      (error) => {
        console.debug('error', error);
      }
    );
  }

  convertGunYPosition(param: {
    currentPosition: number,
    defaultPosition: number,
    down: boolean,
    up: boolean,
    min: number,
    max: number,
    steps: number,
    default: boolean
  }) {

    if (param.default === true) {
      return param.defaultPosition;
    }

    if (param.down === true && param.currentPosition > param.min) {
      return Number((param.currentPosition - param.steps).toFixed(4));
    }

    if (param.up === true && param.currentPosition < param.max) {
      return Number((param.currentPosition + param.steps).toFixed(4));
    }

    return param.currentPosition;
  }

  convertGunXPosition(param: {
    currentPosition: number,
    defaultPosition: number,
    left: number,
    right: number,
    min: number,
    max: number,
    factor: number,
    default: boolean
  }) {

    if (param.default === true) {
      return param.defaultPosition;
    }

    if (param.left > 0 && param.currentPosition > param.min) {
      const newValue = Number((param.currentPosition - (param.left * param.factor)).toFixed(4));
      if (newValue < param.min) {
        return param.min;
      }
      return newValue;
    }

    if (param.right > 0 && param.currentPosition < param.max) {
      const newValue = Number((param.currentPosition + (param.right * param.factor)).toFixed(4));
      if (newValue > param.max) {
        return param.max;
      }
      return newValue;
    }

    return param.currentPosition;
  }

  convertMotorSpeed(position: number): MotorWheelInterface {
    let direcetion: MotorDirectionEnum = MotorDirectionEnum.Stop;
    let speed: number = 0;

    if (position < -0.2) {
      speed = position * -1;
      direcetion = MotorDirectionEnum.Forward;
    }
    else if (position > 0.2) {
      speed = position;
      direcetion = MotorDirectionEnum.Backward;
    }
    else {
      speed = 0;
      direcetion = MotorDirectionEnum.Stop;
    }


    return {
      speed: speed,
      direction: direcetion
    };
  }

  ngOnDestroy() {
    this.ps4GamepadSubscription.unsubscribe();
  }
}
