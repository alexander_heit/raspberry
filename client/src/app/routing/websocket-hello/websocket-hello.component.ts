import { Component, OnInit, OnDestroy } from '@angular/core';
import { WebsocketService } from '../../shared/service/websocket.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-websocket-hello',
  templateUrl: './websocket-hello.component.html',
  styleUrls: ['./websocket-hello.component.scss']
})
export class WebsocketHelloComponent implements OnDestroy {

  private webSocketSubject;
  public connected:Boolean = false;
  public serverMessages: Array<string> = [];
  constructor(private webSocketService: WebsocketService) {

  }

  connect(){
    this.webSocketSubject = this.webSocketService.connect('websocket-hello');
    this.connected = true;
    this.webSocketSubject.subscribe(message => {
      this.serverMessages.push(JSON.parse(message.data));
    }, 
    (err) => {
      this.connected = false;
    }, 
    () => {
      this.connected = false;
    });
  }

  sayHello() {
    this.webSocketSubject.next(JSON.stringify('hello'));
  }

  reset() {
    this.serverMessages = [];
  }

  close(){
    this.connected = false;
    this.webSocketService.close();
  }

  ngOnDestroy() {
     this.webSocketService.close();
  }

}
