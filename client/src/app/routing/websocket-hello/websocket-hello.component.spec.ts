import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocketHelloComponent } from './socket-hello.component';

describe('SocketHelloComponent', () => {
  let component: SocketHelloComponent;
  let fixture: ComponentFixture<SocketHelloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocketHelloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocketHelloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
