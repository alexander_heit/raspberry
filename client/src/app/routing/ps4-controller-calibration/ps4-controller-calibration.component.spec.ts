import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ps4ControllerCalibrationComponent } from './ps4-controller-calibration.component';

describe('Ps4ControllerCalibrationComponent', () => {
  let component: Ps4ControllerCalibrationComponent;
  let fixture: ComponentFixture<Ps4ControllerCalibrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ps4ControllerCalibrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ps4ControllerCalibrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
