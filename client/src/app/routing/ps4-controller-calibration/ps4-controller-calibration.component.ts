import { Component, OnInit, OnDestroy } from '@angular/core';
import { Ps4GamepadService } from '../../shared/service/ps4-gamepad.service';
import { Subscription } from 'rxjs';
import { Ps4GamepadDto } from '../../../../../shared-class/ps4-gamepad.dto';
import { PwmClass } from '../../shared/class/pwm.class';

@Component({
  selector: 'app-ps4-controller-calibration',
  templateUrl: './ps4-controller-calibration.component.html',
  styleUrls: ['./ps4-controller-calibration.component.scss']
})
export class Ps4ControllerCalibrationComponent implements OnDestroy {

  private ps4GamepadObservable:Subscription;
  public gamepad:Ps4GamepadDto = null;
  public Object = Object;
  constructor(private ps4GamepadService:Ps4GamepadService) {
    this.ps4GamepadObservable = this.ps4GamepadService.get(120).subscribe((gamepad) => {
      gamepad.axe.right.x = PwmClass.map(gamepad.axe.right.x, {minIn:-1, maxIn:1, minOut:0.01, maxOut:1});
      gamepad.axe.right.y = PwmClass.map(gamepad.axe.right.y, {minIn:-1, maxIn:1, minOut:0.01, maxOut:1});
      gamepad.axe.left.x = PwmClass.map(gamepad.axe.left.x, {minIn:-1, maxIn:1, minOut:0.01, maxOut:1});
      gamepad.axe.left.y = PwmClass.map(gamepad.axe.left.y, {minIn:-1, maxIn:1, minOut:0.01, maxOut:1});
      this.gamepad = gamepad;  
    });
   }

  ngOnDestroy() {
   this.ps4GamepadObservable.unsubscribe();
  }

}
