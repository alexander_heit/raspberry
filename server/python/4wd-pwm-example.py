#!/usr/bin/env python3
 
"""
File: skidsteer_two_pwm_test.py
 
This code will test Raspberry Pi GPIO PWM on four GPIO
pins. The code test ran with L298N H-Bridge driver module connected.
 
Website:	www.bluetin.io
Date:		27/11/2017
"""
 
__author__ = "Mark Heywood"
__version__ = "0.1.0"
__license__ = "MIT"
 
from gpiozero import PWMOutputDevice
from gpiozero import DigitalOutputDevice
from time import sleep
 
#///////////////// Define Motor Driver GPIO Pins /////////////////
# Motor A, Left Side GPIO CONSTANTS
PWM_DRIVE_LEFT = 14		
FORWARD_LEFT_PIN = 4	
REVERSE_LEFT_PIN = 15	
driveLeft = PWMOutputDevice(PWM_DRIVE_LEFT, True, 0, 1000)
 
forwardLeft = PWMOutputDevice(FORWARD_LEFT_PIN)
reverseLeft = PWMOutputDevice(REVERSE_LEFT_PIN)

 
def allStop():
	forwardLeft.value = False
	reverseLeft.value = False
	driveLeft.value = 0
 
def forwardDrive():
	forwardLeft.value = True
	reverseLeft.value = False
	driveLeft.value = 1.0
 
def reverseDrive():
	forwardLeft.value = False
	reverseLeft.value = True	
	driveLeft.value = 1.0
 
def spinLeft():
	forwardLeft.value = False
	reverseLeft.value = True
	driveLeft.value = 1.0
 
def SpinRight():
	forwardLeft.value = True
	reverseLeft.value = False
	driveLeft.value = 1.0
 
def forwardTurnLeft():
	forwardLeft.value = True
	reverseLeft.value = False
	driveLeft.value = 0.2
 
def forwardTurnRight():
	forwardLeft.value = True
	reverseLeft.value = False
	driveLeft.value = 0.8
 
def reverseTurnLeft():
	forwardLeft.value = False
	reverseLeft.value = True
	driveLeft.value = 0.5

def reverseTurnRight():
	forwardLeft.value = False
	reverseLeft.value = True
	driveLeft.value = 1.0
 
def main():
	reverseTurnLeft()
	sleep(10)
    reverseTurnRight()
    sleep(10)
	allStop()
 
 
if __name__ == "__main__":
    """ This is executed when run from the command line """
    main()