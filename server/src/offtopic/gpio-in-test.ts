import {Gpio, GpioDirection} from '../app/class/gpio';

let gpio = new Gpio(18, GpioDirection.In);

gpio.export().then(() => {
gpio.watch((value) => {
    console.debug('value', value);
})
})


setTimeout(() => {
    console.debug('cleanUp');
    gpio.cleanUp();
}, 30000);

/*
gpio.export(() => {
    console.debug('export');
    gpio.watch((value) => {
        console.debug('value', value);
    });
}, (err) => {
    console.debug('error', err);
});



*/