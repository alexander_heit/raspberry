import {Gpio, GpioDirection} from '../app/class/gpio';

let gpio17 = new Gpio(17, GpioDirection.Out);

gpio17.export(() => {
    console.debug('export');
    gpio17.set(true);
}, (err) => {
    console.debug('error', err);
});

setTimeout(() => {
    console.debug('cleanUp');
    gpio17.cleanUp();
}, 5000);
