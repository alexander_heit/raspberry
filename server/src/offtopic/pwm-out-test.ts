import {PiBlaster} from '../app/class/pi-blaster';

let led = new PiBlaster(14);

led.enable();

led.set(0.3);

setTimeout(() => {
  led.set(0.2);
},5000);

setTimeout(() => {
    led.set(0.4);
  },10000);

  setTimeout(() => {
    led.set(0.99);
  },15000);
