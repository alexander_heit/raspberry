import {FourWheelDriveMotor, MotorDirection} from '../app/class/four-wheel-drive-motor';

let motor = new FourWheelDriveMotor({
    leftSpeedGpio: 14,
    leftForwardGpio: 15,
    leftBackwardGpio: 18,
    rightSpeedGpio: 5,
    rightForwardGpio: 13,
    rightBackwardGpio: 6
});

motor.init().subscribe(() => {
    console.debug('motor start export');
    motor.left(1, MotorDirection.Forward);
    motor.right(1, MotorDirection.Forward);
},
error => {
    console.debug('motor init error', error);
});

setTimeout(() => {
    motor.stop();
}, 2000);