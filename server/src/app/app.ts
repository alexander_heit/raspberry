import { routes } from "./routing/routes";
import { middleware } from "./routing/middleware";

class App {

    static init(readyCallback, port: number = 3000) {
        let app = require('express')();
        require('express-ws')(app);
        middleware(app);
        routes(app);
        app.listen(port, readyCallback);
    }
}

App.init(() => {
    console.log('server listen on port: 3000 ' + (new Date()));
});