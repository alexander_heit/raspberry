import { Request, Response } from 'express';
var path = require('path');

export class HomeController {
    public static Home(req: Request, res: Response) {
        console.log(path.join('/home/pi/projects/raspberry/server/src/app/static/index.html'));
        res.sendFile(path.join('/home/pi/projects/raspberry/server/src/app/static/index.html'));
        //res.send(JSON.stringify('ready home-controller'));
    }
}
