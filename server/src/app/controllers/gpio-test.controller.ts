import {Gpio} from "../class/gpio";

export class GpiotestController {

    public GpioTest = (ws, req) => {
        console.debug('Command websocket ready');
        ws.on('message', (msg) => {           
            ws.send(JSON.stringify({ state: 'ready' }));
        });

        ws.on('close', () => {
            console.log('Verbindung wurde getrennt');
        })
    }
}