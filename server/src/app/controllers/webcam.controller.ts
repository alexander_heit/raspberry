import { WebcamConfigInterface, WebcamConfigDto } from './../../../../shared-class/webcam.dto';

var raspividStream = require('raspivid-stream');

export class WebcamController {
    public webcamWebsocket = null;
    public webcamConfig: WebcamConfigInterface = null;
    public defaultWebcamConfig = {
        width: 960,
        height: 540,
        bitrate: 10000000
    }

    public init = (ws, config: WebcamConfigInterface = {}) => {

        config = { ...this.defaultWebcamConfig, ...config }

        ws.send(JSON.stringify({
            action: 'init',
            width: config.width,
            height: config.height
        }));

        console.debug('config', config);
        let videoStream = raspividStream(config);

        videoStream.on('data', (data) => {
            ws.send(data, { binary: true }, (error) => { if (error) console.error(error); });
        });

        ws.on('close', () => {
            videoStream.removeAllListeners('data');
            this.webcamWebsocket=null;
        });
    }

    public webcamStream = (ws, req) => {
        this.webcamWebsocket = ws;
        console.debug('webcamconfig', this.webcamConfig);
        if (this.webcamWebsocket && this.webcamConfig) {
            this.init(this.webcamWebsocket, this.webcamConfig);
        }
    }

    public commandStream = (ws, req) => {
        console.debug('Command websocket ready');
        ws.on('message', (msg) => {
            this.webcamConfig = <WebcamConfigInterface>JSON.parse(msg);
            if (this.webcamWebsocket && this.webcamConfig) {
                this.init(this.webcamWebsocket, this.webcamConfig);
            }
            ws.send(JSON.stringify({ state: 'ready' }));
        });

        ws.on('close', () => {
            console.log('Verbindung wurde getrennt');
        })
    }
}