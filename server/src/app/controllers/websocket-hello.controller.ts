export class WebsocketHelloController {
    public static WebsocketHello(ws, req) {
        ws.on('message', (msg) => {
            ws.send(JSON.stringify('echo: ' + msg + ' Date:' + (new Date())));
        });

        ws.on('close', () => {
            console.log('Verbindung wurde getrennt');
        })
    }
}