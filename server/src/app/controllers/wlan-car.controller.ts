import { TankControlInterface } from '../../../../shared-class/tankcontrol.dto';
import { FourWheelDriveMotor } from '../class/four-wheel-drive-motor';
import { Gun } from '../class/gun';
import { forkJoin } from 'rxjs';
import { Gpio, GpioDirection } from '../class/gpio';
import { Helper } from '../class/helper';

export class WlanCarController {

    private motor:FourWheelDriveMotor;
    private gun:Gun;
    private hitPin:Gpio;

    constructor() {
        this.motor = new FourWheelDriveMotor({
            leftSpeedGpio: 17,
            leftForwardGpio: 15,
            leftBackwardGpio: 18,
            rightSpeedGpio: 5,
            rightForwardGpio: 13,
            rightBackwardGpio: 6
        });

        this.gun = new Gun({
            xGpio: 24, 
            yGpio: 23,
            fireGpio:26
        });

        this.hitPin = new Gpio(16, GpioDirection.In);
    }
    
    hit = (ws, req) => {
        let lastValue = 0;
        this.hitPin.export().then(() => {
            this.hitPin.watch((value) => {
                if(value === lastValue){
                    return;
                }
                lastValue = value;
                ws.send(JSON.stringify(value));
            });
        });

        ws.on('close', () => {
            console.debug('close');
            this.hitPin.cleanUp().catch(Helper.cleanUpErrorHandler);
        });
    }
    
    command = (ws, req) => {        
        console.debug('command start');
        forkJoin(
            this.motor.init(),
            this.gun.init(),
        ).subscribe(() => {
            ws.send(JSON.stringify('ready'));
            console.debug('motor ready');
        }, (err) => {console.debug('error', err)})

        ws.on('message', (tankControl:TankControlInterface) => {
            tankControl = JSON.parse(<any>tankControl);
            this.motor.left(tankControl.motor.left);
            this.motor.right(tankControl.motor.right);
            this.gun.setX(tankControl.gun.x);
            this.gun.setY(tankControl.gun.y);
            this.gun.fire(tankControl.gun.fire);
        });

        ws.on('close', () => {
            console.debug('close');
           this.motor.cleanUp();
            this.gun.cleanUp();
        });
    }
}