import { PiBlaster } from '../class/pi-blaster';

export class PwmCalibrationController {

    public PwmCalibration = (ws, req) => {
        let pwm = new PiBlaster(14);

        ws.on('message', (msg) => {
            console.debug('msg', msg);
            pwm.set(msg);
        });

        ws.on('close', () => {
            console.log('Verbindung wurde getrennt');
        })

    }
}