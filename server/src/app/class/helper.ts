import { PiBlaster } from "./pi-blaster";
import { Gpio } from "./gpio";

export class Helper{

    public static initPiBlaster(gpio: PiBlaster): Promise<PiBlaster> {
        return new Promise((resolve, reject) => {
            gpio.enable().then(() =>{
                gpio.set(1).then(()=> {
                    resolve(gpio);
                }).catch(err => reject(err))
            }).catch(err => reject(err));
        });
    }

    public static initGpio(gpio: Gpio): Promise<Gpio> {
        return new Promise((resolve, reject) => {
            gpio.export().then(() =>{
                gpio.set(false).then(() =>{
                    resolve(gpio);
                }).catch(err => reject(err))
            }).catch(err => reject(err));
        });
    }

    public static defaultErrorHandler(){
        return err => {console.debug('error', err)};
    }
    
    public static cleanUpErrorHandler(){
        return Helper.defaultErrorHandler();
    }
}