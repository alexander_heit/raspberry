import { File } from './file';
import { rejects } from 'assert';

export enum GpioDirection {
    In = 'in',
    Out = 'out',
}

export class Gpio {

    private gpio: number;
    private direction: GpioDirection;
    private poller;
    private fd;
    private readonly exportPath = '/sys/class/gpio/export';
    private readonly unexportPath = '/sys/class/gpio/unexport';

    constructor(gpio: number, direction: GpioDirection) {
        this.gpio = gpio;
        this.direction = direction;
    }

    private getDirectionPath():string {
        return '/sys/class/gpio/gpio' + this.gpio + '/direction';
    }

    getGpioNumber():number {
        return this.gpio;
    }

    export(): Promise<void> {
        return new Promise((resolve, reject) => {
            let exportMethod = () => {
                File.writeLine(this.exportPath, this.gpio).then(() => {
                    File.writeLine(this.getDirectionPath(), this.direction).then(() => {
                        resolve();
                    }).catch(err => {reject(err);});
                }).catch(err => {reject(err);});
            }
                        
            this.cleanUp().then(exportMethod).catch(exportMethod);
        });
    }

    set(value: boolean): Promise<void> {
        return File.writeLine('/sys/class/gpio/gpio' + this.gpio + '/value', value === true ? "1" : "0");
    }

    cleanUp(): Promise<void> {
        if(this.poller){
            this.poller.remove(this.fd).close();
            this.poller = undefined;
        } 
        return File.writeLine(this.unexportPath, this.gpio);
      
    }

    watch(callback) {
        let re = File.watch('/sys/class/gpio/gpio' + this.gpio + '/value', 1, (rawData) => {
            let rawStr = rawData.toString();
            callback(rawStr == '1' ? 1 : 0);
        });
        this.poller = re.poller;
        this.fd = re.fd;
    }

}