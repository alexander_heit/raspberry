import {File} from './file';

export class PiBlaster{
    
    private gpio:number;

    constructor(gpio:number){
        this.gpio = gpio;
    }

    getGpioNumber(){
        return this.gpio;
    }

    enable(): Promise<void>{
        return this.command(1);
    }

    cleanUp(): Promise<void>{
        return this.command(0);
    }

    set(frequenz:number | string){
        return this.command(frequenz);
    }

    command(command:number | string): Promise<void>{
        return File.writeLine('/dev/pi-blaster', this.gpio + '=' + command);
    }

}
