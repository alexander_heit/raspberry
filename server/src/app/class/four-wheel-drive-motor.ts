import { PiBlaster } from './pi-blaster';
import { Gpio, GpioDirection } from './gpio';
import { forkJoin, Observable } from 'rxjs';
import { Helper } from './helper';
import { MotorDirectionEnum, MotorWheelInterface, MotorInterface } from '../../../../shared-class/tankcontrol.dto';

interface WheelPins{
    speed:PiBlaster;
    forward:Gpio;
    backward:Gpio;
}

export interface GpioPins{
    leftSpeedGpio: number;
    leftForwardGpio: number;
    leftBackwardGpio: number;
    rightSpeedGpio: number;
    rightForwardGpio: number;
    rightBackwardGpio: number;
}

export interface Motor {
    left: {
        direction: MotorDirectionEnum;
        speed: number;
    }
    right: {
        direction: MotorDirectionEnum;
        speed: number;
    }
}

export class FourWheelDriveMotor {

    private _left:WheelPins;
    private _right:WheelPins;
    
    constructor(pins: GpioPins) {
        this._left = {
            speed: new PiBlaster(pins.leftSpeedGpio),
            forward: new Gpio(pins.leftForwardGpio, GpioDirection.Out),
            backward: new Gpio(pins.leftBackwardGpio, GpioDirection.Out)
        };

        this._right = {
            speed: new PiBlaster(pins.rightSpeedGpio),
            forward: new Gpio(pins.rightForwardGpio, GpioDirection.Out),
            backward: new Gpio(pins.rightBackwardGpio, GpioDirection.Out)
        }
    }

    public init(): Observable<any> {
        return forkJoin(
            Helper.initPiBlaster(this._left.speed),
            Helper.initGpio(this._left.forward),
            Helper.initGpio(this._left.backward),
            Helper.initPiBlaster(this._right.speed),
            Helper.initGpio(this._right.forward),
            Helper.initGpio(this._right.backward),
        );
    }

    private setWheel(wheelPins:WheelPins, wheel:MotorWheelInterface){
        wheelPins.speed.set(wheel.speed);
        switch(wheel.direction){
            case MotorDirectionEnum.Backward:
            wheelPins.backward.set(true).catch(Helper.defaultErrorHandler());
            wheelPins.forward.set(false).catch(Helper.defaultErrorHandler());
            break;
   
            case MotorDirectionEnum.Forward:
            wheelPins.forward.set(true).catch(Helper.defaultErrorHandler());
            wheelPins.backward.set(false).catch(Helper.defaultErrorHandler());
            break;

            case MotorDirectionEnum.Stop:
            wheelPins.forward.set(false).catch(Helper.defaultErrorHandler());
            wheelPins.backward.set(false).catch(Helper.defaultErrorHandler());
            break;
        }
    }

    public left(wheel:MotorWheelInterface){
        this.setWheel(this._left, wheel);
    }

    public right(wheel:MotorWheelInterface){
        this.setWheel(this._right, wheel);
    }

    public stop(){
        this.setWheel(this._right, {speed:0, direction:MotorDirectionEnum.Stop});
        this.setWheel(this._left, {speed:0, direction:MotorDirectionEnum.Stop});
    }

    public cleanUp(){
        this._left.speed.cleanUp().catch(Helper.cleanUpErrorHandler());
        this._left.forward.cleanUp().catch(Helper.cleanUpErrorHandler());
        this._left.backward.cleanUp().catch(Helper.cleanUpErrorHandler());
        this._right.speed.cleanUp().catch(Helper.cleanUpErrorHandler());
        this._right.forward.cleanUp().catch(Helper.cleanUpErrorHandler());
        this._right.backward.cleanUp().catch(Helper.cleanUpErrorHandler());
    }
}
