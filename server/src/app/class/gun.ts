import { forkJoin, Observable } from 'rxjs';
import { PiBlaster } from "./pi-blaster";
import { Helper } from "./helper";
import { Gpio, GpioDirection } from './gpio';

export interface GpioPins{
    xGpio: number,
    yGpio: number,
    fireGpio: number,
}

export class Gun{
    
    private xGpio: PiBlaster;
    private yGpio: PiBlaster;
    private fireGpio:Gpio;

    constructor(config:GpioPins){
        this.xGpio = new PiBlaster(config.xGpio);
        this.yGpio = new PiBlaster(config.yGpio);
        this.fireGpio = new Gpio(config.fireGpio, GpioDirection.Out);
    }

    public init(): Observable<any> {
        return forkJoin(
            Helper.initPiBlaster(this.xGpio),
            Helper.initPiBlaster(this.yGpio),
            Helper.initGpio(this.fireGpio),
        );
    }
    
    public setX(position:number){
        this.xGpio.set(position).catch(Helper.defaultErrorHandler());
    }

    public setY(position:number){
        this.yGpio.set(position).catch(Helper.defaultErrorHandler());
    }

    public fire(fire:boolean){
        this.fireGpio.set(fire).catch(Helper.defaultErrorHandler());
    }
    

    public cleanUp(){
        this.yGpio.cleanUp().catch(Helper.cleanUpErrorHandler());
        this.xGpio.cleanUp().catch(Helper.cleanUpErrorHandler());
        this.fireGpio.cleanUp().catch(Helper.cleanUpErrorHandler());
    }
}