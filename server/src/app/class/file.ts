import { open, write, close, openSync, readSync } from 'fs';
import { Epoll } from 'epoll';

export class File {

    public static write(path: string, command: any) :Promise<void> {
        return new Promise((resolve, reject) => {
            let fd = open(path, 'w', undefined, (err, fd) => {
                if (err) {
                    reject(err);
                }
                let buffer = new Buffer(command);
                write(fd, buffer, 0, buffer.length, -1, (error, written, buffer) => {
                    if (error) {
                        reject(error);
                    }
                    close(fd, (err) => {
                        if (err) {
                            reject(err);
                        }
                        resolve();
                    });
                });
            });
        });
    }

    public static writeLine(path, command):Promise<void> {
        return File.write(path, command + '\n');
    }

    public static watch(path, size, callback) {

        const valuefd = openSync(path, 'r');
        const buffer = Buffer.alloc(size);
        const poller = new Epoll((err, fd, events) => {
            readSync(fd, buffer, 0, size, 0);
            callback(buffer);
        });

        poller.add(valuefd, Epoll.EPOLLIN);

        return { poller: poller, fd: valuefd };
    }
}