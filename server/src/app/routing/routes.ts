import { HomeController } from "../controllers/home.controller";
import { WebsocketHelloController} from "../controllers/websocket-hello.controller";
import { WebcamController} from "../controllers/webcam.controller";
import { PwmCalibrationController } from "../controllers/pwm-calibration.controller";
import { WlanCarController } from "../controllers/wlan-car.controller";


export let routes = (app) => {

    let wc = new WebcamController();
    let pwm = new PwmCalibrationController();
    let wlanCar = new WlanCarController();
    app.route('/').get(HomeController.Home);
    app.ws('/websocket-hello', WebsocketHelloController.WebsocketHello);   
    app.ws('/webcam/stream', wc.webcamStream); 
    app.ws('/webcam/command', wc.commandStream);
    app.ws('/pwm-calibration', pwm.PwmCalibration);
    app.ws('/wlan-car/hit', wlanCar.hit);
    app.ws('/wlan-car/command', wlanCar.command);
}