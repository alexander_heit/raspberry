import { Application } from 'express';
const cors = require('cors');

export let middleware = (app: Application) => {
    app.use(cors({
        origin: 'http://localhost:4200',
        optionsSuccessStatus: 200
    }));

}