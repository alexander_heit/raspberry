import {Dto} from './dto.class'
export interface Ps4GamepadDtoInterface {
    button: {
        x: boolean,
        square: boolean,
        triangle: boolean,
        circle: boolean,
        left: boolean,
        right: boolean
    };

    stick: {
        up: boolean,
        down: boolean,
        left: boolean,
        right: boolean
    };

    trigger: {
        l2: number,
        r2: number,
    };

    axe: {
        left: {
            x: number,
            y: number,
        },
        right: {
            x: number,
            y: number
        }
    };
}

export class Ps4GamepadDto extends Dto implements Ps4GamepadDtoInterface {
    public button = {
        x: false,
        square: false,
        triangle: false,
        circle: false,
        left: false,
        right: false
    };

    public stick = {
        up: false,
        down: false,
        left: false,
        right: false
    };

    public trigger = {
        l2: 0,
        r2: 0,
    };

    public axe = {
        left: {
            x: 0,
            y: 0,
        },
        right: {
            x: 0,
            y: 0
        }
    };

    constructor(gamepad: Gamepad) {
        super();
        this.button = {
            x: gamepad.buttons[0].pressed,
            square: gamepad.buttons[2].pressed,
            triangle: gamepad.buttons[3].pressed,
            circle: gamepad.buttons[1].pressed,
            left: gamepad.buttons[4].pressed,
            right: gamepad.buttons[5].pressed
        };

        this.stick = {
            up: gamepad.buttons[12].pressed,
            down: gamepad.buttons[13].pressed,
            left: gamepad.buttons[14].pressed,
            right: gamepad.buttons[15].pressed
        },

        this.trigger = {
            l2: gamepad.buttons[6].value,
            r2: gamepad.buttons[7].value,
        };

        this.axe = {
            left:{
                x: gamepad.axes[0],
                y: gamepad.axes[1],
            },
            right:{
                x:gamepad.axes[2],
                y:gamepad.axes[3]
            }
        };
    }
}