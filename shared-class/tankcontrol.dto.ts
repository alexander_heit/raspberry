import { Dto } from './dto.class';

export enum MotorDirectionEnum {
    Forward = 1,
    Backward = 2,
    Stop = 3,
}

export interface MotorWheelInterface {
    speed:number;
    direction:MotorDirectionEnum;
}

export interface MotorInterface {
    left:MotorWheelInterface;
    right:MotorWheelInterface;
}

export interface GunInterface {
    x:number;
    y:number;
    fire:boolean;
}

export interface TankControlInterface {
    motor:MotorInterface;
    gun:GunInterface;
}

export class TankControlDto extends Dto implements TankControlInterface{
    constructor(
        public motor:MotorInterface = {
            left:{
                speed:0,
                direction:MotorDirectionEnum.Stop
            },
            right:{
                speed:0,
                direction:MotorDirectionEnum.Stop
            },
        }, 
        public gun:GunInterface = {
            x: 0,
            y: 0,
            fire: false,
        }){
        super();
    }
}
