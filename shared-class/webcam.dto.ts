export enum ExposureEnum{
  auto = 'auto',
  night = 'night',
  nightpreview = 'nightpreview',
  backlight = 'backlight',
  spotlight = 'spotlight',
  sports = 'sports',
  snow = 'snow',
  beach = 'beach',
  verylong = 'verylong',
  fixedfps = 'fixedfps',
  antishake = 'antishake',
  fireworks = 'fireworks'
}

export enum AwbEnum{
  off = 'off',
  auto = 'auto',
  sun = 'sun',
  cloud = 'cloud',
  shade = 'shade',
  tungsten = 'tungsten',
  fluorescent = 'fluorescent',
  incandescent = 'incadescent',
  flash = 'flash',
  horizon = 'horizon'
}

export enum ImxfxEnum {
  none = 'none',
  negative = 'negative',
  solarise = 'solarise',
  sketch = 'sketch',
  denoise = 'denoise',
  emboss = 'emboss',
  oilpaint = 'oilpaint',
  hatch = 'hatch',
  gpen = 'gpen',
  pastel = 'pastel',
  watercolour = 'watercolour',
  film = 'film',
  blur = 'blur',
  saturation = 'saturation',
  colourswap = 'colourswap',
  washedout = 'washedout',
  posterise = 'posterise',
  colourpoint = 'colourpoint',
  colourbalance = 'colurbalance',
  cartoon = 'cartoon'
}

export enum MeteringEnum{
  average = 'average',
  spot = 'spot',
  backlit = 'backlit',
  matrix = 'matrix'
}

export interface WebcamConfigInterface{
    width?:number;      // : camera width
    height?:number;     // : camera height  
    sharpness?:number;  // : Set image sharpness (-100 to 100)
    contrast?:number;   // : Set image contrast (-100 to 100)
    brightness?:number; // : Set image brightness (0 to 100)
    saturation?:number; // : Set image saturation (-100 to 100)
    iso?:string;        // : Set capture ISO
    vstab?:number;      // : Turn on video stabilisation
    ev?:number;         // : Set EV compensation
    exposure?:ExposureEnum;   // : Set exposure mode (see Notes)
    awb?:AwbEnum;        // : Set AWB mode (see Notes)
    imxfx?:ImxfxEnum;      // : Set image effect (see Notes)
    colfx?:string;      // : Set colour effect (U:V)
    metering?:MeteringEnum;   // : Set metering mode (see Notes)
    rotation?:string;   // : Set image rotation (0-359)
    hflip?:boolean;      // : Set horizontal flip
    vflip?:boolean;      // : Set vertical flip
    roi?:string;        // : Set region of interest (x,y,w,d as normalised coordinates [0.0-1.0])
    shutter?:number;    // : Set shutter speed in microseconds
  }

  export class WebcamConfigDto implements WebcamConfigInterface{
      constructor(
        public width:number = 960,
        public height:number = 540,
        public sharpness:number = undefined,
        public contrast:number = undefined,
        public brightness:number = undefined,
        public saturation:number = undefined,
        public iso:string = undefined,
        public vstab:number = undefined,
        public ev:number = 0,
        public exposure:ExposureEnum = undefined,
        public awb:AwbEnum = undefined,
        public imxfx:ImxfxEnum = undefined,
        public colfx:string = undefined,
        public metering:MeteringEnum = undefined,
        public rotation:string = undefined,
        public hflip:boolean = undefined,
        public vflip:boolean = undefined,
        public roi:string = undefined,
        public shutter:number = undefined,
      ){
    
      }
  }
